//
//  CookiePatternList.swift
//  CookieKiller
//
//  Created by Gustavo Almeida on 6/6/14.
//  Copyright (c) 2014 galmeida. All rights reserved.
//

import Foundation
import CoreData

class CookiePatternList {
    let managedObjectContext: NSManagedObjectContext
    let dao: CookiePatternDao<CookiePattern>

    init(managedObjectContext managedContext: NSManagedObjectContext, dao: CookiePatternDao<CookiePattern>) {
        self.managedObjectContext = managedContext
        self.dao = dao
    }

    func entries() -> [CookiePattern] {
        return dao.listAll()
    }

    func matchDomain(domain:String, andName name:String) -> Bool {

        if dao.findByDomain(domain, andName: "*") != nil {
            return true
        }
        if dao.findByDomain("*", andName: name) != nil {
            return true;
        }
        if dao.findByDomain(domain, andName: name) != nil {
            return true
        }
        return false;
    }

    func splitEntry(entry:String) -> (domain:String, name:String)? {
        let components = entry.componentsSeparatedByString("/")
        if components.count == 2 && countElements(components[0]) > 0 && countElements(components[1]) > 0 {
            return (components[0], components[1])
        }
        return nil
    }


    func addEntry(entry: String) -> Bool {
        if let components = splitEntry(entry) {
            if dao.findByDomain(components.domain, andName: components.name) == nil {
                addEntryForDomain(components.domain, andName: components.name)
            }
            return true
        }
        return false
    }

    func addEntryForDomain(domain:String, andName name: String) {
        let entry = dao.createEntity()
        entry.domain = domain
        entry.name = name
    }


    func removeEntryForDomain(domain: String, andName name: String) {
        if let entry = dao.findByDomain(domain, andName: name) {
            dao.remove(entry)
        }
    }

    func removeEntry(entry: String) -> Bool {
        if let components = splitEntry(entry) {
            if dao.findByDomain(components.domain, andName: components.name) != nil {
                removeEntryForDomain(components.domain, andName: components.name)
                return true
            }
        }
        return false
    }
    
}