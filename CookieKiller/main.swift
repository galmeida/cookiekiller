//
//  main.swift
//  CookieKiller
//
//  Created by Gustavo Almeida on 6/6/14.
//  Copyright (c) 2014 galmeida. All rights reserved.
//

import Foundation
import CoreData

func managedObjectContext() -> NSManagedObjectContext? {

    let context = NSManagedObjectContext()
    let managedObjectModel = NSManagedObjectModel.mergedModelFromBundles(nil)
    let coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)

    context.persistentStoreCoordinator  = coordinator

    let directories = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask) as [NSURL]
    let url = directories[0].URLByAppendingPathComponent("CookieKiller 1.0.sqlite")

    let options = [ NSMigratePersistentStoresAutomaticallyOption: true,
        NSInferMappingModelAutomaticallyOption: true]

    var possibleError:NSError?

    let store = coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: options, error: &possibleError)

    if let error = possibleError {
        NSLog("Store configuration failure: %@", (error.localizedDescription != nil ? error.localizedDescription : "Unkown Error"))
    }

    return context;
}


func main() {


    if let context = managedObjectContext() {
        let encoding = NSString.defaultCStringEncoding()
        let processor = CookiesProcessor(managedObjectContext: context)
        let wl = WhiteList(managedObjectContext: context)
        let bl = BlackList(managedObjectContext: context)

        let args = NSProcessInfo.processInfo().arguments as [String]
        var run = true
        for var i=1; i<args.count; i++ {
            switch args[i] {
            case "-n":
                processor.useNotificationCenter = true
            case "-v":
                processor.verboseMode = true
            case "-a":
                if let maxAge = args[++i].toInt() {
                    processor.maxCookieAge = maxAge
                } else {
                    println("Invalid max age: \(args[i])")
                    run = false

                }
            case "-d":
                for control in CookieControlDao(managedObjectContext: context).listAll() {
                    println(control)
                }
            case "-l":
                run = false
                println("= white list:")
                for entry in wl.entries() {
                    println("+ \(entry)")
                }
                println("\n= black list:")
                for entry in bl.entries() {
                    println("- \(entry)")
                }
            case "-s":
                run = false
                println("#!/bin/sh\n# \(NSDate())\n# white list")
                for entry in wl.entries() {
                    println("CookieKiller -w \(entry)")
                }
                println("# black list")
                for entry in bl.entries() {
                    println("CookieKiller -b \(entry)")
                }
            case "-w":
                run = false
                let entry = args[++i]
                if !wl.addEntry(entry) {
                    println("Unable to add \(entry) to whitelist")
                }
            case "-W":
                run = false
                let entry = args[++i]
                if !wl.removeEntry(entry) {
                    println("Unable to remove \(entry) from whitelist")
                }
            case "-b":
                run = false
                let entry = args[++i]
                if !bl.addEntry(entry) {
                    println("Unable to add \(entry) to blacklist")
                }
            case "-B":
                run = false
                let entry = args[++i]
                if !bl.removeEntry(entry) {
                    println("Unable to remove \(entry) from blacklist")
                }
            default:
                run = false
                let argv0 = args[0]
                println("usage: \(argv0) [options]")
                println("\t-n               use notification center")
                println("\t-v               verbose mode")
                println("\t-l               show current whitelist")
                println("\t-s               save current white and black lists as a shell script")
                println("\t-w <pattern>     add entry to whitelist.")
                println("\t-W <pattern>     remove entry from whitelist")
                println("\t-b <pattern>     add entry to blacklist. blacklist takes precedence over whitelist")
                println("\t-B <pattern>     remove entry from blacklist")
                println("\t-a <age>         override default maximum cookie age in seconds (default: \(processor.maxCookieAge))")

                println("\npattern format:")
                println("\t<domain>/<name>")
                println("\t<domain>/*       matches anything from a given domain.")
                println("\t*/<name>         matches the named within any domain.")

                println("\nsample config:")

                println("\t* keep cookies from duckduckgo.com:")
                println("\t\(argv0) -w duckduckgo.com/*\n")

                println("\t* add a specific cookie to whitelist:")
                println("\t\(argv0) -w .app.net/mt_uic\n")

                println("\t* blacklist google analytics cookies:")
                println("\t\(argv0) -b */___utmv -b */___utmx -b */__utma -b */__utmb -b */__utmc -b */__utmz")

            }

        }

        if run {
            processor.run()
        }

        var error:NSError?
        if !context.save(&error) {
            let msg = "Error saving data: " + (error?.localizedDescription != nil ? error!.localizedDescription : "Unkown Error")
            NSLog(msg)

            if processor.useNotificationCenter {
                let note = NSUserNotification()
                note.title = "Error"
                note.informativeText = msg
                NSUserNotificationCenter.defaultUserNotificationCenter().deliverNotification(note)
            }

        }

    }
}


main()

