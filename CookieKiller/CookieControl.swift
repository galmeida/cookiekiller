//
//  CookieControl.swift
//  CookieKiller
//
//  Created by Gustavo Almeida on 6/6/14.
//  Copyright (c) 2014 galmeida. All rights reserved.
//

import Foundation
import CoreData

class CookieControl: NSManagedObject, Printable {
    @NSManaged var domain: String?
    @NSManaged var name: String?
    @NSManaged var date: NSDate?
    @NSManaged var lastSeen: NSDate?

    override var description: String {
        return "date: \(date), lastSeen: \(lastSeen)\t\(domain)/\(name)"
    }
    
}