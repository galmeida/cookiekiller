//
//  Dao.swift
//  CookieKiller
//
//  Created by Gustavo Almeida on 6/6/14.
//  Copyright (c) 2014 galmeida. All rights reserved.
//

import Foundation
import CoreData

protocol Dao {
    typealias EntityType : NSManagedObject
    func createEntity() -> EntityType
    func remove(entity:EntityType )
    func defaultSort() -> [NSSortDescriptor]
    func listAll() -> [EntityType]
}
