//
//  CookiePatternDao.swift
//  CookieKiller
//
//  Created by Gustavo Almeida on 6/6/14.
//  Copyright (c) 2014 galmeida. All rights reserved.
//

import Foundation
import CoreData

class CookiePatternDao<T: CookiePattern> : BaseDao<T> {

    init(entityName name: String, managedObjectContext: NSManagedObjectContext) {
        super.init(entityName: name, managedObjectContext: managedObjectContext)
    }
    
    override func defaultSort() -> [NSSortDescriptor] {
        return [ NSSortDescriptor(key: "domain", ascending: true),
                 NSSortDescriptor(key: "name", ascending: true) ]
    }

    func findByDomain(domain: String, andName name: String) -> T? {
        let request = NSFetchRequest()
        let entity = NSEntityDescription.entityForName(entityName, inManagedObjectContext: managedObjectContext)
        request.entity = entity
        request.predicate = NSPredicate(format: "domain == %@ AND name == %@", domain, name)
        
        var possibleError: NSError?
        
        var result = managedObjectContext.executeFetchRequest(request, error: &possibleError) as [T]
        
        if let error = possibleError {
            NSLog("Fetch Error: \(error.localizedDescription)");
        } else {
            if !result.isEmpty {
                return result[0]
            }
        }
        return nil
    }
   
}

