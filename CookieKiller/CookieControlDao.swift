//
//  CookieControlDao.swift
//  CookieKiller
//
//  Created by Gustavo Almeida on 6/6/14.
//  Copyright (c) 2014 galmeida. All rights reserved.
//

import Foundation
import CoreData

class CookieControlDao<T: CookieControl> : BaseDao<T> {

    init(managedObjectContext: NSManagedObjectContext) {
        super.init(entityName: "CookieControl", managedObjectContext: managedObjectContext)
    }


    override func defaultSort() -> [NSSortDescriptor] {
        return [ NSSortDescriptor(key: "date", ascending: true) ]
    }

    func findByDomain(domain:String, andName name:String) -> T? {
        let request = NSFetchRequest()
        let entity = NSEntityDescription.entityForName(entityName, inManagedObjectContext: managedObjectContext)
        request.entity = entity
        request.predicate = NSPredicate(format: "domain == %@ AND name == %@", domain, name)

        let sortDescriptors = self.defaultSort()
        if sortDescriptors.count > 0 {
            request.sortDescriptors = sortDescriptors
        }

        var possibleError: NSError?
        let result = managedObjectContext.executeFetchRequest(request, error: &possibleError)

        if let error = possibleError {
            NSLog("Fetch Error: \(error.localizedDescription)");
        } else {
            if result.count > 0 {
                return result[0] as? T
            }
        }
        return nil
    }

    func findNotSeenSince(date:NSDate) -> [T] {
        let request = NSFetchRequest()
        let entity = NSEntityDescription.entityForName(entityName, inManagedObjectContext: managedObjectContext)
        request.entity = entity
        request.predicate = NSPredicate(format: "lastSeen < %@", date);

        let sortDescriptors = defaultSort()
        if sortDescriptors.count > 0 {
            request.sortDescriptors = sortDescriptors
        }

        var possibleError: NSError?
        let result = managedObjectContext.executeFetchRequest(request, error: &possibleError) as [T]

        if let error = possibleError {
            NSLog("Fetch Error: \(error.localizedDescription)");
        } else {
            return result
        }
        return []
    }
}