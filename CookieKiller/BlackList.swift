//
//  BlackList.swift
//  CookieKiller
//
//  Created by Gustavo Almeida on 6/6/14.
//  Copyright (c) 2014 galmeida. All rights reserved.
//

import Foundation
import CoreData

class BlackList : CookiePatternList {
    init(managedObjectContext managedContext: NSManagedObjectContext) {
        super.init(managedObjectContext: managedContext, dao: BlackListEntryDao(managedObjectContext: managedContext))
    }
}