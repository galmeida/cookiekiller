//
//  BaseDao.swift
//  CookieKiller
//
//  Created by Gustavo Almeida on 6/6/14.
//  Copyright (c) 2014 galmeida. All rights reserved.
//

import Foundation
import CoreData

class BaseDao<T: NSManagedObject> : Dao {
    typealias EntityType = T
    let entityName: String
    let managedObjectContext: NSManagedObjectContext

    init(entityName: String, managedObjectContext: NSManagedObjectContext) {
        self.entityName = entityName
        self.managedObjectContext = managedObjectContext
        assert(entityName != nil)
        assert(entityName != "")
    }

    func createEntity() -> T {
        return NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: managedObjectContext) as T
    }

    func remove(entity:T) {
        managedObjectContext.deleteObject(entity)
    }

    func defaultSort() -> [NSSortDescriptor] {
        return []
    }

    func  listAll() -> [T] {
        let request = NSFetchRequest()
        let entity = NSEntityDescription.entityForName(entityName, inManagedObjectContext: managedObjectContext)
        request.entity = entity;

        let sortDescriptors = defaultSort()
        if sortDescriptors.count > 0 {
            request.sortDescriptors = sortDescriptors;
        }

        var possibleError:NSError?

        var result = managedObjectContext.executeFetchRequest(request, error: &possibleError)
        if let error = possibleError {
            NSLog("Fetch Error: \(error.localizedDescription)");
            return []
        }

        return result as [T]
    }
}