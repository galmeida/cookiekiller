//
//  File.swift
//  CookieKiller
//
//  Created by Gustavo Almeida on 6/6/14.
//  Copyright (c) 2014 galmeida. All rights reserved.
//

import Foundation
import CoreData

class BlackListEntryDao<T: CookiePattern> : CookiePatternDao<T> {
    init(managedObjectContext: NSManagedObjectContext)  {
        super.init(entityName: "BlackListEntry", managedObjectContext: managedObjectContext)
    }
}
