//
//  CookiesProcessor.swift
//  CookieKiller
//
//  Created by Gustavo Almeida on 6/6/14.
//  Copyright (c) 2014 galmeida. All rights reserved.
//

import Foundation
import CoreData

class CookiesProcessor {
    let managedObjectContext:NSManagedObjectContext
    var useNotificationCenter = false
    var verboseMode = false
    var maxCookieAge =  24 * 60 * 60
    var now = NSDate.date()

    //TODO nao deveria precisar usar o <>
    let cookieDao:CookieControlDao<CookieControl>
    
    init(managedObjectContext:NSManagedObjectContext) {
        self.managedObjectContext = managedObjectContext;
        self.cookieDao = CookieControlDao(managedObjectContext: managedObjectContext)
    }
    
    func killCookies() -> Int {
        let cookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
        let cookies = cookieStorage.cookies as [NSHTTPCookie]
        let whitelist = WhiteList(managedObjectContext: managedObjectContext)
        let blacklist = BlackList(managedObjectContext: managedObjectContext)
        let now = NSDate.date()
        
        var count = 0
        
        for cookie in cookies {
            var reason = ""
            let control = self._getControlFor(cookie)
            let delta = now.timeIntervalSinceDate(control.date)
            if Int(delta) >= maxCookieAge {
                var shouldRemove = true
                var blacklisted = false
                var whitelisted = whitelist.matchDomain(cookie.domain, andName: cookie.name)
                if whitelisted {
                    blacklisted = blacklist.matchDomain(cookie.domain, andName: cookie.name)
                    if blacklisted {
                        reason = " (backlisted)"
                    } else {
                        shouldRemove = false
                        reason = " (whitelisted)"
                    }
                }
                
                if shouldRemove {
                    if verboseMode {
                        NSLog("removing \(cookie.domain)/\(cookie.name), age=\(delta)\(reason)")
                    }
                    cookieStorage.deleteCookie(cookie)
                    cookieDao.remove(control)
                    count++
                } else {
                    if verboseMode {
                        NSLog("skiping \(cookie.domain)/\(cookie.name), age=\(delta)\(reason)")
                    }
                }
            }
        }

        return count
    }
    
    func notifyUser(count: Int) {
        let msg = "\(count) cookie(s) removed"
        
        if verboseMode || count > 0 {
            NSLog(msg);
        }
        
        if useNotificationCenter && count > 0 {
            let notification = NSUserNotification()
            notification.title = msg;
            NSUserNotificationCenter.defaultUserNotificationCenter().deliverNotification(notification)
        }

    }
    
    func removeUselessCookieControl() {
        let notSeen = cookieDao.findNotSeenSince(now)
        for cookieControl in notSeen {
            println(cookieControl)
            cookieDao.remove(cookieControl);
        }
    }
    
    func run() {
        if verboseMode {
            NSLog("max cookie age: %ld", maxCookieAge)
        }
        var cookiesKilled = self.killCookies()
        self.notifyUser(cookiesKilled)
        self.removeUselessCookieControl()
    }
    
    func _getControlFor(cookie: NSHTTPCookie) -> CookieControl {
        var result:CookieControl
        if let control = cookieDao.findByDomain(cookie.domain, andName: cookie.name) {
            result = control
        } else {
            var date: NSDate
            //try to get creation date from cookie
            if let created = cookie.properties["Created"] as? NSNumber {
                date = NSDate(timeIntervalSinceReferenceDate: created.doubleValue)
            } else {
                date = now
            }

            result = cookieDao.createEntity()
            result.name = cookie.name
            result.domain = cookie.domain
            result.date = date
        }
        result.lastSeen = now
        return result;
    }
}