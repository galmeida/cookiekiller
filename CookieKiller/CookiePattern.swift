//
//  CookiePattern.swift
//  CookieKiller
//
//  Created by Gustavo Almeida on 6/6/14.
//  Copyright (c) 2014 galmeida. All rights reserved.
//

import Foundation
import CoreData

class CookiePattern : NSManagedObject, Printable {
    @NSManaged var domain: String
    @NSManaged var name: String
    
    override var description: String {
        return "\(domain)/\(name)"

    }
}